<!DOCTYPE html>
<html lang="en">

<?php $title="Accueil"; include('inc/head.inc.php')?>
<body>

<?php include('inc/header.inc.php') ?>

<!-- Le contenu de la page-->
<div class="container py-3">

<div class="row">
<div class="col-md-4">
<div class="card text-white bg-primary mb-3">
  <div class="card-body">
    <h5 class="card-title">999</h5>
    <p class="card-text">Messages envoyés</p>
  </div>
</div>
</div>


<div class="col-md-4">
<div class="card text-white bg-success mb-3">
  <div class="card-body">
    <h5 class="card-title">999</h5>
    <p class="card-text">Etudiants enregistrés</p>
  </div>
</div>
</div>


<div class="col-md-4">
<div class="card text-white bg-warning mb-3">
  <div class="card-body">
    <h5 class="card-title">10</h5>
    <p class="card-text">Classes</p>
  </div>
</div>
</div>
</div>

</div>
<?php include('inc/foot.inc.php')?>

</body>

</html>
