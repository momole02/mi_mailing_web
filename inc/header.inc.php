<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
<div class="container">
    <a class="navbar-brand" href="#"><img src="img/logo-light.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#">Accueil
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Messages</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Classes</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Etudiants</a>
        </li>
    </ul>
    </div>
</div>
</nav>
